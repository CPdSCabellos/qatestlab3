package myprojects.automation.assignment3;

import myprojects.automation.assignment3.utils.Logger;
import myprojects.automation.assignment3.utils.Properties;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Contains main script actions that may be used in scripts.
 */
public class GeneralActions {
    private WebDriver driver;
    private WebDriverWait wait;
    private JavascriptExecutor js;
    private Actions actions;

    public GeneralActions(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 30);
        js = (JavascriptExecutor)driver;
        actions = new Actions(driver);
    }

    /**
     * Logs in to Admin Panel.
     * @param login
     * @param password
     */
    public void login(String login, String password) {
        driver.navigate().to(Properties.getBaseAdminUrl());

        By loginFieldLocator = By.xpath("//input[@id='email']");

        wait.until(ExpectedConditions.presenceOfElementLocated(loginFieldLocator));

        WebElement loginField = driver.findElement(loginFieldLocator);
        loginField.clear();
        loginField.sendKeys(login);

        WebElement passwordField = driver.findElement(By.xpath("//input[@id='passwd']"));
        passwordField.clear();
        passwordField.sendKeys(password);

        WebElement loginBtn = driver.findElement(By.xpath("//button[@name='submitLogin']"));
        loginBtn.submit();

        waitForContentLoad();
    }

    /**
     * Adds new category in Admin Panel.
     * @param categoryName
     */
    public void createCategory(String categoryName) {
        WebElement ordersBtn = driver.findElement(By.xpath("//li[@id='subtab-AdminCatalog']"));
        actions.moveToElement(ordersBtn).build().perform();

        WebElement categoriesBtn = driver.findElement(By.xpath("//li[@id='subtab-AdminCategories']"));

        wait.until(ExpectedConditions.visibilityOf(categoriesBtn));
        categoriesBtn.click();

        waitForContentLoad();

        WebElement addNewCategoryBtn = driver.findElement(By.xpath("//*[@id='page-header-desc-category-new_category']"));
        addNewCategoryBtn.click();

        waitForContentLoad();

        WebElement categoryNameField = driver.findElement(By.xpath("//input[@id='name_1']"));
        categoryNameField.clear();
        categoryNameField.sendKeys(categoryName);

        scrollToBottom();

        WebElement saveBtn = driver.findElement(By.xpath("//button[@id='category_form_submit_btn']"));
        saveBtn.submit();

        waitForContentLoad();

        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@class='alert alert-success']")));

        scrollToBottom();

        WebElement backBtn = driver.findElement(By.xpath("//a[@id='desc-category-back']"));
        backBtn.click();

        waitForContentLoad();
    }

    public void findCategoryFromTable(String categoryName){
        WebElement filterField = driver.findElement(By.xpath("//input[@name='categoryFilter_name']"));
        filterField.clear();
        filterField.sendKeys(categoryName);

        waitForContentLoad();

        WebElement filterBtn = driver.findElement(By.xpath("//button[@id='submitFilterButtoncategory']"));
        filterBtn.submit();

        waitForContentLoad();

        wait.until(ExpectedConditions
                .visibilityOf(driver
                        .findElement(By.xpath("//button[@name='submitResetcategory']"))));

        WebElement categoryElement = driver.findElement(By.xpath("//table[@id='table-category']/tbody/tr/td[2]"));

        categoryElement.click();

        waitForContentLoad();

        WebElement pageTitle = driver.findElement(By.xpath("//h2[@class='page-title']"));

        String text = pageTitle.getText().trim();

        if(text.equals(categoryName)){
            Logger.print("PASS!!!!");
        } else {
            Logger.print("ERROR! Expected category["+categoryName+"] but get ["+text+"]");
        }
    }

    private void scrollToBottom(){
        js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
    }

    /**
     * Waits until page loader disappears from the page
     */
    public void waitForContentLoad() {
        wait.until(ExpectedConditions
                .attributeContains(By.xpath("//*[@id='ajax_running']"), "style", "display: none;"));
    }

}
