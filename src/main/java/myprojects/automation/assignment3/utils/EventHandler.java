package myprojects.automation.assignment3.utils;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.WebDriverEventListener;

import java.util.Arrays;

public class EventHandler implements WebDriverEventListener {

    @Override
    public void beforeAlertAccept(WebDriver webDriver) {
        Logger.print("Before alert accept ["+webDriver+"]");
    }

    @Override
    public void afterAlertAccept(WebDriver webDriver) {
        Logger.print("After alert accept ["+webDriver+"]");
    }

    @Override
    public void afterAlertDismiss(WebDriver webDriver) {
        Logger.print("After alert dismiss ["+webDriver+"]");
    }

    @Override
    public void beforeAlertDismiss(WebDriver webDriver) {
        Logger.print("Before alert dismiss ["+webDriver+"]");
    }

    @Override
    public void beforeNavigateTo(String s, WebDriver webDriver) {
        Logger.print("Before navigate to ["+s+"]");
    }

    @Override
    public void afterNavigateTo(String s, WebDriver webDriver) {
        Logger.print("After navigate to ["+s+"]");
    }

    @Override
    public void beforeNavigateBack(WebDriver webDriver) {
        Logger.print("Before navigate back ["+webDriver+"]");
    }

    @Override
    public void afterNavigateBack(WebDriver webDriver) {
        Logger.print("After navigate back ["+webDriver+"]");
    }

    @Override
    public void beforeNavigateForward(WebDriver webDriver) {
        Logger.print("Before navigate forward ["+webDriver+"]");
    }

    @Override
    public void afterNavigateForward(WebDriver webDriver) {
        Logger.print("After navigate forward ["+webDriver+"]");
    }

    @Override
    public void beforeNavigateRefresh(WebDriver webDriver) {
        Logger.print("Before navigate refresh ["+webDriver+"]");
    }

    @Override
    public void afterNavigateRefresh(WebDriver webDriver) {
        Logger.print("After navigate to ["+webDriver+"]");
    }

    @Override
    public void beforeFindBy(By by, WebElement webElement, WebDriver webDriver) {
        Logger.print("Before finding by ["+by+"] element ["+webElement+"]");
    }

    @Override
    public void afterFindBy(By by, WebElement webElement, WebDriver webDriver) {
        Logger.print("After finding by ["+by+"] element ["+webElement+"]");
    }

    @Override
    public void beforeClickOn(WebElement webElement, WebDriver webDriver) {
        Logger.print("Before click on element ["+webElement+"]");
    }

    @Override
    public void afterClickOn(WebElement webElement, WebDriver webDriver) {
        Logger.print("After click on element ["+webElement+"]");
    }

    @Override
    public void beforeChangeValueOf(WebElement webElement, WebDriver webDriver, CharSequence[] charSequences) {
        Logger.print("Before change value of element ["+webElement+"] to ["+ Arrays.toString(charSequences) +"]");
    }

    @Override
    public void afterChangeValueOf(WebElement webElement, WebDriver webDriver, CharSequence[] charSequences) {
        Logger.print("After change value of element ["+webElement+"] to ["+ Arrays.toString(charSequences) +"]");
    }

    @Override
    public void beforeScript(String s, WebDriver webDriver) {
        Logger.print("Before script ["+s+"]");
    }

    @Override
    public void afterScript(String s, WebDriver webDriver) {
        Logger.print("After script ["+s+"]");
    }

    @Override
    public void onException(Throwable throwable, WebDriver webDriver) {
        Logger.print("Exception ["+throwable.getMessage()+"]");
    }
}
