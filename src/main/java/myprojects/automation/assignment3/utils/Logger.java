package myprojects.automation.assignment3.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Logger {

    private static final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

    public static void print(String log){
        System.out.println("["+simpleDateFormat.format(new Date(System.currentTimeMillis()))+"]" + log);
    }
}
